package com.barro.s01activity;

public class S01Activity {

    public static void main(String[] args){

        int age = 25;

        String name = "Luffy";

        double power = 100.78;

        boolean isAPirate = true;

        System.out.println("Hello! My name is " + name +  " I am " + age + " years old." + " My power is " + power + " Am I a Pirate? " + isAPirate);

    }
}
